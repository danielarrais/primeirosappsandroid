package com.developer.daniel.startbuzz;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.developer.daniel.startbuzz.classes.Drink;

public class CategoriaDrinks extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ListView listaDrinks = getListView();

        ArrayAdapter<Drink> listaAdapter = new ArrayAdapter<Drink>(
                this, android.R.layout.simple_list_item_1, Drink.drinks
        );

        listaDrinks.setAdapter(listaAdapter);
    }

    public void onListItemClick(ListView listView, View view, int position, long id){
        Intent intent = new Intent(CategoriaDrinks.this, DrinkAtividade.class);
        intent.putExtra(DrinkAtividade.EXTRA_DRINKNO, (int) id);
        startActivity(intent);
    }
}
