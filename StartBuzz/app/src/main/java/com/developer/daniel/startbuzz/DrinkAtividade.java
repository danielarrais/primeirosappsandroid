package com.developer.daniel.startbuzz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.developer.daniel.startbuzz.classes.Drink;

public class DrinkAtividade extends AppCompatActivity {

    public static final String EXTRA_DRINKNO = "drinkNo";
    int id;
    TextView nome, descricao;
    ImageView foto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink_atividade);
setTitle(Drink.drinks[id].getNome());
        nome = (TextView) findViewById(R.id.name);
        descricao = (TextView) findViewById(R.id.descricao);
        foto = (ImageView) findViewById(R.id.fotoCaput);

        id = (Integer) getIntent().getExtras().get(EXTRA_DRINKNO);
        setTitle(Drink.drinks[id].getNome());
        foto.setImageResource(Drink.drinks[id].getIdImage());
        nome.setText(Drink.drinks[id].getNome());
        descricao.setText(Drink.drinks[id].getDescricao());
    }
}
