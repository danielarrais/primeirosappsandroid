package com.developer.daniel.startbuzz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

public class home extends AppCompatActivity {
    TextView texto = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        texto = (TextView) findViewById(R.id.texto);
        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {

                } else if (position == 1) {
                    Intent drinkCategoria = new Intent(home.this, CategoriaDrinks.class);
                    startActivity(drinkCategoria);
                }
                if (position == 2) {
                    texto.setText("Você escolheu Suco");
                }
            }
        };
        ListView listaBebidas = (ListView) findViewById(R.id.beb);
        listaBebidas.setOnItemClickListener(itemClickListener);
    }
}
