package com.developer.daniel.embaralhapalavras.mecanica.Interfaces;
import com.developer.daniel.embaralhapalavras.RecursosExternos.BancoDePalavras;

import java.util.ArrayList;

public interface MecanicaDoJogo {
    public boolean isError();
    public int getAcertos();
    public float getPontos();
    public String getPalavra();
    public String getInfor();
    public BancoDePalavras getBancoDePalavras();
    public String getPalavraEmbalharada();
	public float getValorAcerto();
    public void setLista(ArrayList<String> lista);

    public void setError(boolean Error);
    public void setAcertos();
    public void setPalavraEmbalharada();
    
    public void novaPalavra();
    public boolean perdeu();
    public void jogar(String Resposta);
    public void reiniciarJogo();

}
