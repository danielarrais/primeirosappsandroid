package com.developer.daniel.embaralhapalavras.embaralhador;

import com.developer.daniel.embaralhapalavras.embaralhador.Interfaces.Embaralhador;

import java.io.Serializable;

public class EmbaralhadorAstesrisk implements Embaralhador, Serializable {
	private char[] vogais = { 'A', 'E', 'I', 'O', 'U' };

	public String embaralha(String palavra) {
		palavra = palavra.toUpperCase();
		for (int i = 0; i < vogais.length; i++) {
			String vogal = Character.toString(vogais[i]);
			palavra = palavra.replaceAll(vogal, "*");
		}
		return palavra;
	}
}
