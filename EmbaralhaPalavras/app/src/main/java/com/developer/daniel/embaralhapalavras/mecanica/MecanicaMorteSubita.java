package com.developer.daniel.embaralhapalavras.mecanica;

import java.io.Serializable;
import java.util.ArrayList;

public class MecanicaMorteSubita extends MecanicaPai implements Serializable {
	public MecanicaMorteSubita(ArrayList<String> lista){
		super(lista);
	}
	public String getInfor() {
		return "1. Cada questão vale: " + getValorAcerto() + " pontos"
				+ "\n3. Você não pode errar, se isso acontecer o Jogo encerra "
				+ "\n4. Digite EXIT no lugar da palavra para encerrar o jogo\n";
	}

	public boolean perdeu() {
		return (isError() ? true : false);
	}

}
