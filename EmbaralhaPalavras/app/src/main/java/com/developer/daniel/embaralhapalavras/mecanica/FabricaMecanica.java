package com.developer.daniel.embaralhapalavras.mecanica;

import com.developer.daniel.embaralhapalavras.mecanica.Interfaces.MecanicaDoJogo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

public class FabricaMecanica implements Serializable {
	public static MecanicaDoJogo retornaMecanica(ArrayList<String> lista) {
		Random r = new Random();
		int n = r.nextInt(1);
		switch (n) {
		case 0:
			return new MecanicaMorteSubita(lista);
		case 1:
			return new MecanicaMaisAcertos(lista);
		}
		return null;
	}

}
