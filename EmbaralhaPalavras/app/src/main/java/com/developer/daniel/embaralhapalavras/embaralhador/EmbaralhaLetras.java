package com.developer.daniel.embaralhapalavras.embaralhador;
import com.developer.daniel.embaralhapalavras.embaralhador.Interfaces.Embaralhador;

import java.io.Serializable;
import java.util.Random;

public class EmbaralhaLetras implements Embaralhador, Serializable {
	private static Random r = new Random();

	public String embaralha(String palavra) {
		char array[] = palavra.toCharArray();
		String s = "";
		for (int i = 0; i < (array.length); i++) {
			int aleatorio = r.nextInt(array.length);
			char ant = array[i];
			array[i] = array[aleatorio];
			array[aleatorio] = ant;
		}
		for (int i = 0; i < array.length; i++) {
			s = s + array[i];
		}
		return s;
	}
}
