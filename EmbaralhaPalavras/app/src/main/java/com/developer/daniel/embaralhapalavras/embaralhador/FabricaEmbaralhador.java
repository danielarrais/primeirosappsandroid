package com.developer.daniel.embaralhapalavras.embaralhador;

import com.developer.daniel.embaralhapalavras.embaralhador.Interfaces.Embaralhador;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

public class FabricaEmbaralhador implements Serializable {
	private static ArrayList<Embaralhador> array = new ArrayList<>();
	private static Random r = new Random();

	public static Embaralhador retorno() {
		array.add(new EmbaralhaLetras());
		array.add(new EmbaralhadorAstesrisk());
		return array.get(r.nextInt(array.size()));
	}
}
