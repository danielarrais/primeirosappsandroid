package com.developer.daniel.embaralhapalavras.RecursosExternos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

public class BancoDePalavras implements Serializable {

    //Atributos da classe
    private String nomeArquivo = null;
    private Random vrSorteio = new Random();
    private ArrayList<String> vrPalavras = new ArrayList<>();

    //Construtor da classe
    public BancoDePalavras(ArrayList<String> lista) {
        vrPalavras = lista;
    }

    //Metodo privado utilizado para sortear uma palavra lida do arquivo


    //Metodo utilizado para sortear uma palavra
    public String sorteiaPalavra() {
        //Sorteia uma palavra
        if (vrPalavras.size() > 0) {
            int posicao = vrSorteio.nextInt(vrPalavras.size());
            return vrPalavras.get(posicao);
        }
        return null;
    }
    //Metodo utilizado para sortear uma palavra
    public String sorteiaPalavra(int tamanho) {
        String Palavra="";
        //Sorteia uma palavra com o tamanho passado com argumento
        do {
            Palavra = sorteiaPalavra();
        } while (Palavra.length() != tamanho);
        return Palavra;
    }
}
