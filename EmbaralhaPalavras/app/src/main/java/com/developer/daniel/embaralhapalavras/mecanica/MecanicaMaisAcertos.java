package com.developer.daniel.embaralhapalavras.mecanica;

import java.io.Serializable;
import java.util.ArrayList;

public class MecanicaMaisAcertos extends MecanicaPai implements Serializable {
	private int erros = 0;

	public String getInfor() {
		return "1. Cada questão vale: " + getValorAcerto() + " pontos" + "\n3. Você não pode errar a primeira resposta"
				+ "\n4. Você joga enquanto a quantidade de erro for menor ou igual a de acertos"
				+ "\n4. Digite EXIT no lugar da palavra para encerrar o jogo\n";
	}
	public MecanicaMaisAcertos(ArrayList<String> lista){
		super(lista);
	}
	public boolean perdeu() {
		return (getErros() > getAcertos() ? true : false);
	}

	public int getErros() {
		return erros;
	}

	public void jogar(String Respostas) {
		super.jogar(Respostas);
		if (isError()) {
			setErros();
		}
	}

	public void setErros() {
		this.erros++;
	}

	public void reiniciarJogo() {
		super.reiniciarJogo();
		erros = 0;
	}

	public String toString() {
		return super.toString() + "-------Acertos: " + getAcertos() + "   Erros: " + getErros() + "-------" + "\n"
				+ "-----------------------------------\n";
	}
}
