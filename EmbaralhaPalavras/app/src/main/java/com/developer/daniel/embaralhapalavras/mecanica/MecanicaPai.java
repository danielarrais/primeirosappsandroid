package com.developer.daniel.embaralhapalavras.mecanica;

import com.developer.daniel.embaralhapalavras.RecursosExternos.BancoDePalavras;
import com.developer.daniel.embaralhapalavras.embaralhador.FabricaEmbaralhador;
import com.developer.daniel.embaralhapalavras.embaralhador.Interfaces.Embaralhador;
import com.developer.daniel.embaralhapalavras.mecanica.Interfaces.MecanicaDoJogo;

import java.io.Serializable;
import java.util.ArrayList;

public abstract class MecanicaPai implements MecanicaDoJogo, Serializable {
    private ArrayList<String> lista = new ArrayList<>();
    private BancoDePalavras banco;
    private Embaralhador embaralhador = FabricaEmbaralhador.retorno();
    private String palavra, pEmbalharada;
    private boolean Error;
    private int acertos;
    final float pontosPorAcertos = 10;

    public float getValorAcerto() {
        return pontosPorAcertos;
    }

    public void setLista(ArrayList<String> lista) {

        this.lista = lista;
        banco = new BancoDePalavras(this.lista);
    }

    public MecanicaPai(ArrayList<String> lista) {
        setLista(lista);
        novaPalavra();
    }

    public boolean isError() {
        return Error;
    }

    public int getAcertos() {
        return acertos;
    }

    public void setAcertos() {
        acertos++;
    }

    public float getPontos() {
        return getAcertos() * pontosPorAcertos;
    }

    public abstract String getInfor();

    public String getPalavra() {
        return palavra;
    }

    public BancoDePalavras getBancoDePalavras() {
        return banco;
    }

    public String getPalavraEmbalharada() {
        return pEmbalharada;
    }

    public void setError(boolean Error) {
        this.Error = Error;
    }

    public void setPalavraEmbalharada() {
        pEmbalharada = embaralhador.embaralha(getPalavra());
    }

    public void novaPalavra() {

        this.palavra = banco.sorteiaPalavra();
        setPalavraEmbalharada();
    }

    public void jogar(String Resposta) {
        querSair(Resposta);
        if (Resposta.equalsIgnoreCase(getPalavra())) {
            setError(false);
            setAcertos();
        } else {
            setError(true);
        }
    }

    public abstract boolean perdeu();

    public void reiniciarJogo() {
        acertos = 0;
        setError(false);
    }

    public void querSair(String Resposta) {
        if (Resposta.equalsIgnoreCase("exit")) {
            System.out.println("Jogo Encerrado!!!");
            System.exit(0);
        }
    }
    public String toString() {
        return "-----------------------------------\n" + "------------Pontos: " + getPontos() + "------------" + "\n"
                + "-----------------------------------\n";
    }

}
