package com.developer.daniel.cadastrodeclientenovo;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Home extends AppCompatActivity {

    TextView nome = null;
    TextView endereco = null;
    Spinner sexo = null;
    Spinner idade = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        nome = (TextView) findViewById(R.id.tf_nome);
        endereco = (TextView) findViewById(R.id.tf_endereco);
        sexo = (Spinner) findViewById(R.id.sp_sexo);
        idade = (Spinner) findViewById(R.id.sp_idade);
    }

    public void cadastrar(View v){
        BancoController banco = new BancoController(getBaseContext());

        Cliente cliente = new Cliente(nome.getText().toString(),
                endereco.getText().toString(),
                sexo.getSelectedItem().toString(),
                Integer.valueOf(idade.getSelectedItem().toString()));

        String resultado = banco.inserirCliente(cliente);

        Toast.makeText(getApplicationContext(), resultado, Toast.LENGTH_LONG).show();

        Intent ver =new Intent(this, VisualizarClientes.class);
        ver.putExtra("nome", cliente.getNome());
        startActivity(ver);

    }
}
