package com.developer.daniel.cadastrodeclientenovo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by danie on 13/04/2017.
 */

public class BancoController {
    private SQLiteDatabase db;
    private CriaBanco banco;

    public BancoController(Context context) {
        banco = new CriaBanco(context);
    }

    public String inserirCliente(Cliente c) {
        ContentValues cv = new ContentValues();
        long resultado;

        db = banco.getWritableDatabase();

        cv.put(CriaBanco.NOME, c.getNome());
        cv.put(CriaBanco.ENDERECO, c.getEndereco());
        cv.put(CriaBanco.SEXO, c.getSexo());
        cv.put(CriaBanco.IDADE, c.getIdade());

        resultado = db.insert(CriaBanco.TABELA, null, cv);
        db.close();

        if (resultado ==-1)
            return "Erro ao inserir registro";
        else
            return "Registro Inserido com sucesso";
    }

    public Cursor carregaDados(){
        Cursor cursor;
        String[] campos =  {banco.ID,banco.NOME};
        db = banco.getReadableDatabase();
        cursor = db.query(banco.TABELA, campos, null, null, null, null, null, null);

        if(cursor!=null){
            cursor.moveToFirst();
        }
        db.close();
        return cursor;
    }
}
