package com.developer.daniel.cadastrodeclientenovo;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class VisualizarClientes extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizar_clientes);

        BancoController crud = new BancoController(getBaseContext());
        Cursor cursor = crud.carregaDados();


        TextView cliente = (TextView) findViewById(R.id.nome_cliente);

        String dado = cursor.getString(1);
        cliente.setText(savedInstanceState.getString("nome"));
        cursor.close();
    }
}
