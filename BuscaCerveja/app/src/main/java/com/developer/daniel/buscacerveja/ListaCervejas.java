package com.developer.daniel.buscacerveja;

import java.util.ArrayList;
import java.util.List;

public class ListaCervejas {
    public List<String> listaTipos(String cores) {
        List<String> lista = new ArrayList<String>();

        if (cores.equals("ligth")) {
            lista.add("Ligth Silver");
            lista.add("Dark Write");
        } else if (cores.equals("amber")) {
            lista.add("Wisk Amber");
            lista.add("Amber Stone");
        } else if (cores.equals("brown")) {
            lista.add("Jack Tecquila");
            lista.add("Brown Tequila");
        } else if (cores.equals("dark")) {
            lista.add("Jail Pale");
            lista.add("Gout Soul");
        }
        return lista;
    }
}
