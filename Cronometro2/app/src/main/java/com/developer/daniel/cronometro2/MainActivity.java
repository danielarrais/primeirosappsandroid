package com.developer.daniel.cronometro2;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private int segundos = 0;
    private boolean executando, wasRuning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState != null) {
            segundos = savedInstanceState.getInt("segundos");
            executando = savedInstanceState.getBoolean("contando");
            wasRuning = savedInstanceState.getBoolean("executando");
        }
        rodarOTempo();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("segundos", segundos);
        outState.putBoolean("contando", executando);
        outState.putBoolean("executando", wasRuning);
    }

    @Override
    protected void onResume() {
        super.onStart();
        if (wasRuning) {
            executando = true;
        }
    }

    @Override
    protected void onPause() {
        super.onStop();
        wasRuning = executando;
        executando = false;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void onClickStart(View view) {
        executando = true;
    }

    public void onClickStop(View view) {
        executando = false;
    }

    public void onClickReset(View view) {
        executando = false;
        segundos = 0;
    }

    private void rodarOTempo() {
        final TextView tempo = (TextView) findViewById(R.id.time_view);
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                int horas = segundos / 3600;
                int minutos = (segundos % 3600) / 60;
                int sec = segundos % 60;
                String time = String.format("%d:%02d:%02d", horas, minutos, sec);
                tempo.setText(time);
                if (executando) {
                    segundos++;
                }
                handler.postDelayed(this, 1000);
            }
        });
    }
}
